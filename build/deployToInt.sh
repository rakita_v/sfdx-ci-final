printf 'y' | sfdx plugins:install sfdx-git-delta
sfdx force:auth:jwt:grant --clientid $CONSUMER_KEY --username $SFDC_STAGING_USER --jwtkeyfile build/server.key --setdefaultusername --setalias sfdx-ci --instanceurl $SFDC_PROD_URL
LAST_TAG=`git describe --tags --abbrev=0`
echo "Last deployment tag name: ($LAST_TAG)"

#Generating packages containing deltas from last deployment
sfdx sgd:source:delta --to "staging" --from $LAST_TAG --output .

LAST_TAG="${LAST_TAG/DeployToStaging./}"
NEW_TAG="DeployToStaging.$((LAST_TAG+1))"

echo "New deployment tag name ($NEW_TAG)" 

sfdx force:source:deploy -x package/package.xml -u sfdx-ci || exit
sfdx force:apex:test:run -c -r human -w 10

git tag $NEW_TAG
git push --tags