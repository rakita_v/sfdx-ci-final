#!/bin/bash

echo "Creating scratch org in order to check build."

sfdx force:auth:jwt:grant --clientid $CONSUMER_KEY --username $SFDC_STAGING_USER --jwtkeyfile build/server.key --setdefaultdevhubusername --setalias sfdx-ci --instanceurl $SFDC_PROD_URL
echo "Creating scratch org."
sfdx force:org:create --definitionfile "./config/project-scratch-def.json" -a temporarySO --setdefaultusername
echo "Pushing Metadata into scratch org."
sfdx force:source:push
echo "Running All Unit Tests..."
sfdx force:apex:test:run -c -r human -w 10
echo "Cleaning up temporary scratch org."
sfdx force:org:delete -u temporarySO -p

echo "Operation ended."